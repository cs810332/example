﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Example.ViewModel
{
    public class BMIData
    {
        [Required(ErrorMessage = "Required!!!")]
        [Range(30,50,ErrorMessage ="30~50")]
        [Display(Name ="體重")]
        public float Weight { get; set; }
        [Required(ErrorMessage = "Required!!!")]
        [Range(150,200, ErrorMessage = "150~200")]
        [Display(Name = "身高")]
        public float Height { get; set; }

        public float BMI { get; set; }
        public string Level { get; set; }

    }
}